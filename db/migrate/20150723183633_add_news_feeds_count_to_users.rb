class AddNewsFeedsCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :news_feeds_count, :integer
  end
end

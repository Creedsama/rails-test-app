class CreateNewsFeeds < ActiveRecord::Migration
  def change
    create_table :news_feeds do |t|
      t.string :content
      t.integer :user_id
      t.integer :micropost_id
      t.string :feed_type
      t.timestamps
    end
  end
end

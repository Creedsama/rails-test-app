class MicropostsController < ApplicationController
    before_action :signed_in_user, only: [:create, :destroy, :index, :approve]
    before_action :correct_user,   only: :destroy
    before_action :admin_user,     only: :reject


    def create
        @micropost = current_user.microposts.build(micropost_params)
        @micropost.toggle!(:approved) if current_user.admin?
        if @micropost.save
            flash[:success] = "Micropost created!"
            redirect_to root_url
        else
            @feed_items = []
            render 'static_pages/home'
        end
    end

    def destroy
        @micropost = Micropost.find_by_id(params[:id])
        @micropost.destroy
        redirect_to root_url
    end

    def reject
        @micropost = Micropost.find_by_id(params[:id])
        @micropost.news_feeds.create(content: "Your post has been rejected. Thus, deleted from the system.", feed_type: "Rejected Post", user_id: @micropost.user_id) unless @micropost.user.admin?
        @micropost.destroy
        redirect_to microposts_path
    end

    def approve
        @micropost = Micropost.find_by_id(params[:id])
        @micropost.toggle!(:approved)
        flash[:success] = "Micropost Approved!"
        @micropost.send_notification_to_user
        redirect_to microposts_path
    end

    def index
        @pending_microposts = Micropost.pending.paginate(page: params[:page]) if current_user.admin?
        @published_microposts = Micropost.published.paginate(page: params[:page])
        respond_to do |format|
            format.html
        end
    end

    private

    def micropost_params
        params.require(:micropost).permit(:content)
    end

    def correct_user
        @micropost = current_user.microposts.find_by(id: params[:id])
        redirect_to root_url if @micropost.nil?
    end

    def admin_user
        redirect_to(root_url) unless current_user.admin?
    end
end

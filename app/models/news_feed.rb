class NewsFeed < ActiveRecord::Base
	belongs_to :user, counter_cache: :news_feeds_count
	belongs_to :micropost

	default_scope -> { order('created_at DESC') }

	# after_save :send_notification
	# after_create :send_notification_to_admin

	private

	# def send_notification
	# 	admin = User.find_by(admin: true)
 #        poster = self.user
	# 	if self.feed_type == "Pending Post"
	# 		poster = self.user
	# 		Libnotify.show(body: "#{poster.name} has a new unpublished post.", summary: "New Notification", timeout: 2.5) if !poster.admin?
	# 	elsif self.feed_type == "Approved Post" and !self.user.admin?
	# 		user = self.user
	# 		Libnotify.show(body: "Admin has approved your post. It should be visible now.", summary: "New Notification", timeout: 2.5)
	# 	end
	# end
end

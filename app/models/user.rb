class User < ActiveRecord::Base

    has_many :microposts, dependent: :destroy
    has_many :relationships, foreign_key: "follower_id", dependent: :destroy
    has_many :followed_users, through: :relationships, source: :followed
    has_many :reverse_relationships, foreign_key: "followed_id", class_name:  "Relationship", dependent: :destroy
    has_many :followers, through: :reverse_relationships, source: :follower
    has_many :news_feeds

    before_save { self.email = email.downcase }
    before_create :create_remember_token
    before_create :make_super_admin

    validates :name, presence: true, length: { maximum: 50 }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
    validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
    has_secure_password
    validates :password, length: { minimum: 6 }


    def User.new_remember_token
        SecureRandom.urlsafe_base64
    end

    def User.digest(token)
        Digest::SHA1.hexdigest(token.to_s)
    end

    def feed
        Micropost.published.from_users_followed_by(self)
    end

    def following?(other_user)
        relationships.find_by(followed_id: other_user.id)
    end

    def follow!(other_user)
        relationships.create!(followed_id: other_user.id)
    end

    def unfollow!(other_user)
        relationships.find_by(followed_id: other_user.id).destroy
    end

    def cached_count
        Rails.cache.fetch([self, "users_feed_count"]) { news_feeds.size }
    end

    def flush_cache
        Rails.cache.delete([self, "users_feed_count"])
    end

    private

    def create_remember_token
        self.remember_token = User.digest(User.new_remember_token)
    end

    def make_super_admin
        self.admin = true if User.count < 1
    end
end

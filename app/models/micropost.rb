class Micropost < ActiveRecord::Base

    belongs_to :user

    has_many :news_feeds

    default_scope -> { order('created_at DESC') }

    scope :published, -> { where(approved: true) }
    scope :pending, -> { where(approved: false) }

    validates :content, presence: true, length: { maximum: 140 }
    validates :user_id, presence: true

    after_create :send_notification_to_admin

    auto_html_for :content do
        html_escape
        image
        youtube(:width => 400, :height => 250, :autoplay => true)
        link :target => "_blank", :rel => "nofollow"
        simple_format
    end

    # Returns microposts from the users being followed by the given user.
    def self.from_users_followed_by(user)
        followed_user_ids = "SELECT followed_id FROM relationships WHERE follower_id = :user_id" 
                            where("user_id IN (#{followed_user_ids}) OR user_id = :user_id", user_id: user.id)
    end

    def self.global_feed
        published.order("created_at DESC")
    end

    def send_notification_to_user
        micropost = self
        user = micropost.user
        unless user.blank?
            new_notification = user.news_feeds.create(content: "The Admin has approved your post. It will be visible to you and everyone",
                                                        feed_type: "Approved Post", micropost_id: micropost.id)
            # Libnotify.show(body: "Admin has approved your post. It should be visible now.", summary: "New Notification", timeout: 2.5)
        end
    end

    private

    def send_notification_to_admin
        User.where("admin IS ? ", true).each do |admin|
            # admin = User.find_by(admin: true)
            poster = self.user
            admin.news_feeds.create(content: "A new unpublished post. Please, review it.",
                                    micropost_id: self.id, feed_type: "Pending Post") unless poster.admin?
            # Libnotify.show(body: "#{poster.name} has a new unpublished post.", summary: "New Notification", timeout: 2.5) if !poster.admin?
        end
    end
end
